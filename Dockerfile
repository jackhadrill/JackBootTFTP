FROM alpine:3.16 as build-ipxe

WORKDIR /build
RUN apk add --no-cache git make gcc musl-dev perl xz-dev

RUN git clone https://github.com/ipxe/ipxe.git
WORKDIR /build/ipxe/src
RUN git checkout d72c8fdc902bc5d605fef081a18f6fe84f3d0512

RUN sed -i 's/#define\tKEYBOARD_MAP\tus/#define\tKEYBOARD_MAP\tuk/g' config/console.h 
RUN sed -i 's/\/\/#define REBOOT_CMD/#define REBOOT_CMD/g' config/general.h 
RUN sed -i 's/\/\/#define POWEROFF_CMD/#define POWEROFF_CMD/g' config/general.h 
RUN sed -i 's/\/\/#define PARAM_CMD/#define PARAM_CMD/g' config/general.h

COPY embed.ipxe /build/

RUN make -j8 bin/undionly.kpxe EMBED=/build/embed.ipxe
RUN make -j8 bin/ipxe.kpxe EMBED=/build/embed.ipxe
RUN make -j8 bin/ipxe.lkrn EMBED=/build/embed.ipxe
RUN make -j8 bin-x86_64-efi/ipxe.efi EMBED=/build/embed.ipxe


FROM alpine:3.16

RUN apk add --no-cache dnsmasq

RUN mkdir /tftp && echo -e "\
port=0\n\
enable-tftp\n\
tftp-root=/tftp\
" > /etc/dnsmasq.conf

COPY --from=build-ipxe /build/ipxe/src/bin/undionly.kpxe /tftp/
COPY --from=build-ipxe /build/ipxe/src/bin/ipxe.kpxe /tftp/
COPY --from=build-ipxe /build/ipxe/src/bin/ipxe.lkrn /tftp/
COPY --from=build-ipxe /build/ipxe/src/bin-x86_64-efi/ipxe.efi /tftp/

EXPOSE 69/udp

ENTRYPOINT [ "dnsmasq", "--no-daemon" ]
CMD [ "" ]